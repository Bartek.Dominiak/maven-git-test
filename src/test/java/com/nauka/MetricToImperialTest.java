package com.nauka;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class MetricToImperialTest {

    private MetricToImperial metricToImperial;


    @BeforeEach
    void setUp() {
        this.metricToImperial = new MetricToImperial();
    }

    @ParameterizedTest
    @ValueSource(
            doubles = {-1.75879678567878989E18D, -1000.0D, -7.0D}
    )
    void givenCmToInchesBelowZero_shouldReturnZero(double cm) {
        double result = this.metricToImperial.cmToInches(cm);
        Assertions.assertEquals(0.0D, result);
    }

    @Test
    public void givenCmToInchesZero_shouldReturnZero() {
        Assertions.assertEquals(0.0D, this.metricToImperial.cmToInches(0.0D));
    }

    @Test
    public void givenCmToInchesCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(0.7874015748031495, this.metricToImperial.cmToInches(2.0D));
    }

    @Test
    public void givenCmToInchesCorrectBigNumber_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(35450.00, this.metricToImperial.cmToInches(90043.0D));
    }

    @ParameterizedTest
    @ValueSource(
            doubles = {-1.778979087809325E12D, -1000.0D, -7.0D}
    )
    void givenMetersToFeetBelowZero_shouldReturnZero(double meters) {
        double result = this.metricToImperial.metersToFeet(meters);
        Assertions.assertEquals(0.0D, result);
    }

    @Test
    public void givenMetersToFeetZero_shouldReturnZero() {
        Assertions.assertEquals(0.0D, this.metricToImperial.metersToFeet(0.0D));
    }

    @Test
    public void givenMetersToFeetCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(127.9527559055118, this.metricToImperial.metersToFeet(39.0D));
    }

    @ParameterizedTest
    @ValueSource(
            doubles = {-1.778979087809325E12D, -1000.0D, -7.0D}
    )
    void givenLitersToImpGallonsBelowZero_shouldReturnZero(double litres) {
        double result = this.metricToImperial.litresToImpGallons(litres);
        Assertions.assertEquals(0.0D, result);
    }

    @Test
    public void givenLitresToImpGallonsZero_shouldReturnZero() {
        Assertions.assertEquals(0.0D, this.metricToImperial.litresToImpGallons(0.0D));
    }

    @Test
    public void givenLitresToImpGallonsCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(1.5394098999999999, this.metricToImperial.litresToImpGallons(7.0D));
    }

    @Test
    public void givenLitresToImpGallonsCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(2.199157, this.metricToImperial.litresToImpGallons(10.0D));
    }

    @ParameterizedTest
    @ValueSource(
            doubles = {-1.778979087809325E12D, -1000.0D, -7.0D}
    )
    void givenKilogramsToPoundsBeloweZero_shouldReturnZero(double kilograms) {
        double result = this.metricToImperial.kilogramsToPounds(kilograms);
        Assertions.assertEquals(0.0D, result);
    }

    @Test
    void givenKilogramsToPoundsZero_shouldReturnZero() {
        Assertions.assertEquals(0.0D, this.metricToImperial.kilogramsToPounds(0.0D));
    }

    @Test
    void givenKilogramsToPoundsCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(11.02311310925, this.metricToImperial.kilogramsToPounds(5.0D));
    }

    @Test
    void givenKilogramsToPoundsCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(22.0462262185, this.metricToImperial.kilogramsToPounds(10.0D));
    }

    @Test
    void givenCelsiusToFahrenheitMinusCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(-4.0D, this.metricToImperial.celsiusToFahrenheit(-20.0D));
    }

    @Test
    void givenCelsiusToFahrenheitMinusCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(8.599999999999998D, this.metricToImperial.celsiusToFahrenheit(-13.0D));
    }

    @Test
    void givenCelsiusToFahrenheitZero_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(32.0D, this.metricToImperial.celsiusToFahrenheit(0.0D));
    }

    @Test
    void givenCelsiusToFahrenheitPlusCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(50.0D, this.metricToImperial.celsiusToFahrenheit(10.0D));
    }

    @Test
    void givenCelsiusToFahrenheitPlusCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(212.0D, this.metricToImperial.celsiusToFahrenheit(100.0D));
    }
}
