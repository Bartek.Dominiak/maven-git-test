package com.nauka;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ImperialToMetricTest {

    private ImperialToMetric imperialToMetric;

    @BeforeEach
    void setUp() {
        imperialToMetric = new ImperialToMetric();
    }


    @ParameterizedTest
    @ValueSource(doubles = {-2313.21, -12, -6.32, -1})
    void givenFeetToMetersBelowZero_shouldReturnZero(double feet) {
        double result = imperialToMetric.feetToMeters(feet);
        Assertions.assertEquals(0.00, result);
    }

    @Test
    void givenFeetToMetersZero_shouldReturnZero() {
        Assertions.assertEquals(0.00, imperialToMetric.feetToMeters(0));
    }

    @Test
    void givenFeetToMetersCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(1.524, imperialToMetric.feetToMeters(5));
    }

    @Test
    void givenFeetToMetersCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(3.048, imperialToMetric.feetToMeters(10));
    }

    @ParameterizedTest
    @ValueSource(doubles = {-2313.21, -12, -6.32, -1})
    void givenInchesToCmBelowZero_shouldReturnZero(double inches) {
        double result = imperialToMetric.inchesToCm(inches);
        Assertions.assertEquals(0.00, result);
    }

    @Test
    void givenInchesToCmZero_shouldReturnZero() {
        Assertions.assertEquals(0.00, imperialToMetric.inchesToCm(0));
    }

    @Test
    void givenInchesToCmCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(12.70, imperialToMetric.inchesToCm(5));
    }

    @Test
    void givenInchesToCmCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(25.40, imperialToMetric.inchesToCm(10));
    }

    @ParameterizedTest
    @ValueSource(doubles = {-2313.21, -12, -6.32, -1})
    void givenPoundToKgBelowZero_shouldReturnZero(double pounds) {
        double result = imperialToMetric.poundsToKg(pounds);
        Assertions.assertEquals(0.00, result);
    }

    @Test
    void givenPoundsToKgZero_shouldReturnZero() {
        Assertions.assertEquals(0.00, imperialToMetric.poundsToKg(0));
    }

    @Test
    void givenPoundsToKgCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(2.25, imperialToMetric.poundsToKg(5));
    }

    @Test
    void givenPoundsToKgCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(4.50, imperialToMetric.poundsToKg(10));
    }

    @ParameterizedTest
    @ValueSource(doubles = {-2313.21, -12, -6.32, -1})
    void givenGallonsToLitresBelowZero_shouldReturnZero(double gallons) {
        double result = imperialToMetric.gallonsToLitres(gallons);
        Assertions.assertEquals(0.00, result);
    }

    @Test
    void givenGallonsToLitresZero_shouldReturnZero() {
        Assertions.assertEquals(0.00, imperialToMetric.gallonsToLitres(0));
    }

    @Test
    void givenGallonsToLitresCorrect_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(22.75, imperialToMetric.gallonsToLitres(5));
    }

    @Test
    void givenGallonsToLitresCorrect_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(45.50, imperialToMetric.gallonsToLitres(10));
    }

    @Test
    void givenFahrenheitToCelsiusBelowZero_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(-40.00, imperialToMetric.fahrenheitToCelsius(-40));
    }

    @Test
    void givenFahrenheitToCelsiusBelowZero_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(-35.00, imperialToMetric.fahrenheitToCelsius(-31));
    }

    @Test
    void givenFahrenheitToCelsiusBelowZero_shouldReturnCorrectAnswer3() {
        Assertions.assertEquals(-18.333333333333332, imperialToMetric.fahrenheitToCelsius(-1));
    }

    @Test
    void givenFahrenheitToCelsiusAboveZero_shouldReturnCorrectAnswer() {
        Assertions.assertEquals(0.00, imperialToMetric.fahrenheitToCelsius(32));
    }

    @Test
    void givenFahrenheitToCelsiusAboveZero_shouldReturnCorrectAnswer2() {
        Assertions.assertEquals(100.00, imperialToMetric.fahrenheitToCelsius(212));
    }

}