package com.nauka;

public class ImperialToMetric {

    private static final double FEET_TO_METERS = 0.3048;
    private static final double INCHES_TO_CM = 2.54;
    private static final double POUNDS_TO_KG = 0.45359237;
    private static final double IMPERIAL_GALLONS_TO_LITRES = 4.54609;


    public double feetToMeters(double ft) {
        double meters = ft * FEET_TO_METERS;
        return meters;
    }

    public double inchesToCm(double inches) {
        double cm = inches * INCHES_TO_CM;
        return cm;
    }

    public double poundsToKg (double pounds){
        double kg = pounds * POUNDS_TO_KG;
        return kg;
    }

    public double gallonsToLitres (double gallons){
        double litres = gallons * IMPERIAL_GALLONS_TO_LITRES;
        return litres;
    }

    public double fahrenheitToCelsius (double fahrenheit){
        double celsius = (fahrenheit-32)/1.8;
        return celsius;
    }




}
