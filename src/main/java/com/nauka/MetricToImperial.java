package com.nauka;

public class MetricToImperial {

    private static final double METERS_TO_FEET_CONVERTER = (1 / 0.3048);
    private static final double CM_TO_INCHES = (1 / 2.54);
    private static final double LITRES_TO_IMPERIAL_GALLONS = 0.2199157;
    private static final double KILOGRAMS_TO_POUNDS = 2.20462262185;

    public double metersToFeet(double meters) {
        if (meters > 0) {
            double feet = meters * METERS_TO_FEET_CONVERTER;
            return feet;
        }
        return 0;

    }

    public double cmToInches(double cm) {
        if (cm > 0) {
            double inches = cm * CM_TO_INCHES;
            return inches;
        }
        return 0;

    }

    public double litresToImpGallons(double litres) {
        if (litres > 0) {
            double gallons = litres * LITRES_TO_IMPERIAL_GALLONS;
            return gallons;
        }
        return 0;
    }

    public double kilogramsToPounds(double kilograms) {
        if (kilograms > 0) {
            double pounds = kilograms * KILOGRAMS_TO_POUNDS;
            return pounds;
        }
        return 0;
    }

    public double celsiusToFahrenheit(double celsius) {
        double fahrenheit = celsius * 1.8 + 32.00;
        return fahrenheit;
    }


}
